import java.util.ArrayList;

public class ArrayListExample {

	public static void main(String[] args) {
		// 1. Create a new List
		ArrayList<String>names = new ArrayList<String>();
		
		// 1. Add 2 people to the list
		names.add("Gagan");
		names.add("Harman");
		
		// 2. Output total number of people in list
		System.out.println(names.size());
		
		// 3. Delete someone from the list
		
		// 4. Output all people in the list
		
		
		// 5. Get one person out of the list
		
		// 6. Change the name of the person
		
		// 7. Delete everyone from list
		
		// 8. Loop through every item in the list and output:
		// HELLO _______ (where ___ is the name)
		
	}

}
